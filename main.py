from jira.client import JIRA, GreenHopper

# By default, the client will connect to a JIRA instance started from the Atlassian Plugin SDK
# (see https://developer.atlassian.com/display/DOCS/Installing+the+Atlassian+Plugin+SDK for details).
# Override this with the options parameter.
options = {
  'server': 'https://vidzor.atlassian.net',
}
# jira = JIRA(options, basic_auth=('v', 'totor1980'))
# # import ipdb; ipdb.set_trace()
#
# # Get all projects viewable by anonymous users.
# projects = jira.projects()
#
# # Sort available project keys, then return the second, third, and fourth keys.
# keys = sorted([project.key for project in projects])[2:5]
#
# # Get an issue.
# issue = jira.issue('VIDZOR-858')

gh = GreenHopper(options, basic_auth=('v', 'totor1980'))
print gh.boards()

print gh.sprints(13)
print gh.incompleted_issues(13, 23)

import ipdb; ipdb.set_trace()
