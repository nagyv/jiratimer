JIRA time tracking from your OS
================================

This app provides a simple interface to JIRA with its primary aim being to help
development work to stay in the console, and still provide time tracking and
estimation.

The app has two, basically separable parts. The main part is a daemon process
that runs a simple REST server, and communicates with JIRA. The other part is a
reference CLI interface to communicate with the server.

The daemon can run user notifications as well on a regular basis.

Installation
--------------

    npm install

Configuration
--------------

At the minimum level you should create `local.coffee` under the `config`
directory, and provide your user credentials there. See the local.tpl.coffee
file for an example.

Further configuration is possible. See the `condif/default.coffee` for all the
configarion option. A special configuration value is for `Server.notificationTimer`
setting it to `0` means no notifications. See the config file for more details.

Running the app
----------------

Starting the daemon

    npm start

For individual commands run

    coffee index.coffee --help

Running tests
--------------

Running your tests requires mocha to be installed

    npm install -g mocha
    npm test

License
--------

Copyright (c) 2014, Viktor Nagy <v@pulilab.com>


Permission to use, copy, modify, and/or distribute this software for any purpose with or without fee is hereby granted, provided that the above copyright notice and this permission notice appear in all copies.

THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
