restify = require('restify')
CFG =require('config')
logger = require 'winston'
storage = require './storage'
_ = require 'lodash'

if CFG.Server.notificationTimer > 0
  require './notifier'

if storage.getItem 'accessToken'
  CFG.OAuth.access_token = storage.getItem 'accessToken'
  CFG.OAuth.access_token_secret = storage.getItem 'accessTokenSecret'
  console.log "Trying with OAuth"
else
  CFG.OAuth = false

jiratimer = require('./jiratimer')(CFG.JIRA, CFG.User, CFG.OAuth)

server = restify.createServer()
server.use(restify.queryParser());

server.get "/storage/:key", (req, res, next) ->
  if _.isUndefined storage.getItem(req.params.key)
    logger.debug "No active timer found"
    next(new Error("No active timer found"))
  else
    res.send(storage.getItem(req.params.key))
    next()

server.get "/issues", (req, res, next) ->
  jiratimer.searchIssues CFG.Issues, 'Rank ASC', (err, resp) ->
    if err
      next(err)
    else
      res.send(resp)
      next()

server.get "/issues/in-progress", (req, res, next) ->
  args = _.clone CFG.Issues
  args.assignee = CFG.User.login
  args.status = "In Progress"

  jiratimer.searchIssues args, 'Rank ASC', (err, resp) ->
    if err
      next(err)
    else
      res.send(resp)
      next()

server.get "/issues/:issueKey", (req, res, next) ->
  jiratimer.getIssue req.params.issueKey, (err, resp) ->
    if err
      next(err)
    else
      res.send(resp)
      next()

server.get "/issues/:issueKey/transitions", (req, res, next) ->
  jiratimer.jira.listTransitions req.params.issueKey, (err, resp) ->
    if err
      next(err)
    else
      res.send(resp)
      next()

server.get "/issues/:issueKey/start", (req, res, next) ->
  jiratimer.startWork req.params.issueKey, (err, resp) ->
    if err
      next(err)
    else
      res.send(resp)
      next()

server.get "/issues/:issueKey/stop", (req, res, next) ->
  req.query.new ?= false
  jiratimer.stopWork req.params.issueKey, req.query.new, (err, resp) ->
    if err
      next(err)
    else
      res.send(resp)
      next()

server.listen CFG.Server.port, CFG.Server.interface,
  ->
    storage.setItem 'daemonPid', process.pid
    logger.info("#{server.name} listening at #{server.url}")
