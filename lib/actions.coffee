logger = require 'winston'
program = require 'commander'
CFG =require('config')
ui = require('./ui')
_ = require 'lodash'
http = require("http")

callDaemon = (path, cb) ->
  #The url we want is: 'www.random.org/integers/?num=1&min=1&max=10&col=1&base=10&format=plain&rnd=new'
  options =
    host: CFG.Server.interface
    port: CFG.Server.port
    path: path

  callback = (response) ->
    str = ""
    #another chunk of data has been recieved, so append it to `str`
    response.on "data", (chunk) ->
      str += chunk
      return
    #the whole response has been recieved, so we just print it out here
    response.on "end", ->
      resp = JSON.parse(str)
      if response.statusCode < 400
        cb(null, resp)
      else
        cb(resp)
    return

  http.request(options, callback).end()

showResponse = (err, resp) ->
  if err
    console.log "Error:", err
  else if not resp.status
    console.log resp
  else
    console.log(resp.status)
    _.each resp.messages, (message) ->
      console.log message

handleSingleIssue = (err, issue) =>
    callDaemon "/issues/#{issue.key}/transitions", (err, tlist) =>
      if err
        logger.error(err)
      else
        ui.events.on "startWork", () ->
          callDaemon "/issues/#{issue.key}/start", showResponse

        ui.events.on "stopWork", () ->
          callDaemon "/issues/#{issue.key}/stop", showResponse

        ui.events.on "transition", (tId) ->
          callDaemon "/issues/#{issue.key}/transitionTo/#{tId}", showResponse

        ui.showActionPage issue, tlist

stopWork = (key, remaining=false) ->
  remaining = if remaining then "?new=#{remaining}" else ""
  callDaemon "/issues/#{key}/stop#{remaining}", showResponse

rl = require("readline")
ask = (question, callback) ->
  r = rl.createInterface(
    input: process.stdin
    output: process.stdout
  )
  r.question question + "\n", (answer) ->
    r.close()
    callback null, answer
    return

  return

# All these methods are called as methodname.apply(jiratimer, ...)
module.exports =
  oauth: ->
    fs = require "fs"
    OAuth = require "oauth"
    
    CFG.OAuth.consumer_secret = fs.readFileSync CFG.OAuth.cert_key
    consumer = new OAuth.OAuth(
          "https://vidzor.atlassian.net/plugins/servlet/oauth/request-token", 
          "https://vidzor.atlassian.net/plugins/servlet/oauth/access-token",
          CFG.OAuth.consumer_key, CFG.OAuth.consumer_secret, "1.0", null, "RSA-SHA1");
    consumer.getOAuthRequestToken (error, oauthToken, oauthTokenSecret, results) ->
      if error
        console.log(error.data);
      else
        console.log("Please, visit")
        console.log("\thttps://vidzor.atlassian.net/plugins/servlet/oauth/authorize?oauth_token="+oauthToken);
        console.log(program.Command.prototype.confirm)
        ask 'Have you approved? (y/n)', (err, ok) ->
          if ok == "y"
            consumer.getOAuthAccessToken oauthToken, oauthTokenSecret, null, (err, accessToken, accessTokenSecret, results) ->
              if err
                console.log "Access token error", err
              else
                storage = require './storage'
                storage.setItem 'accessToken', accessToken
                storage.setItem 'accessTokenSecret', accessTokenSecret
          else
            console.log "Not approved"

  
  # retrieves the default set of issues, and show the resulting issue list
  kanban: ->
    logger.info("Retrieving top issues")
    callDaemon '/issues',
      (error, issues) =>
        if error
          logger.error(error)
        else
          ui.showIssueSelection issues, handleSingleIssue

  show: (key) ->
    callDaemon "/issues/#{key}", handleSingleIssue

  info: () ->
    callDaemon '/storage/current', (err, current) ->
      if err
        console.log err.message
      else
        console.log "Timer found for key #{current.key}."
        console.log "Timer started on #{new Date(unix_timestamp=current.started)}"

  current: () ->
    callDaemon '/issues/in-progress',
      (error, issues) =>
        if error
          logger.error(error)
        else
          if issues.total == 0
            console.log("No running issues found")
          else if issues.total == 1
            handleSingleIssue null, issues.issues[0]
          else
            ui.showIssueSelection issues, handleSingleIssue

  stopWork: (key, args) ->
    args[0] ?= null  # this is the remaining time
    stopWork key, args[0]?

  start: (key) ->
    callDaemon "/issues/#{key}/start", showResponse

  stop: (args) ->
    args[0] ?= null  # this is the remaining time
    callDaemon '/storage/current', (err, current) ->
      if err
        console.log = err.message
      else
        stopWork current.key, args[0]
