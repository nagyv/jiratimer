CFG =require('config')
createMenu = require('terminal-menu')
_ = require("lodash")
EventEmitter = require('events').EventEmitter

events = new EventEmitter()
module.exports.events = events

# Shows the actions available for a given issue
# On selection, the respecite jiratimer method is called
module.exports.showActionPage = showActionPage = (issue, tlist) ->
    actionsMenu = createMenu()
    actionsMenu.write("#{issue.fields.summary} (#{issue.key})\n")
    actionsMenu.write("\n#{issue.fields.description}\n")
    actionsMenu.write("Assignee: #{issue.fields.assignee?.displayName}\n")
    actionsMenu.write("Status: #{issue.fields.status.name}\n")
    if issue.fields.subtasks
      actionsMenu.write("Subtasks:\n")
      _.each issue.fields.subtasks,
        (i) ->
          actionsMenu.write("\t#{i.key}\n")
    actionsMenu.write("Select an action:\n\n")
    actionsMenu.on "select", (label) -> actionsMenu.close()
    actionsMenu.add "Start work",
      (label, menu) =>
        events.emit('startWork')
        return

    actionsMenu.add "Stop work",
      (label, menu) =>
        events.emit('stopWork')
        return

    if CFG.Transitions.showTransitions
      actionsMenu.write "Transition to\n"
      _.each tlist,
        (trans) =>
          actionsMenu.add "\t#{trans.name}",
            (label, menu) =>
              events.emit('transition', trans.id)
              return

    actionsMenu.add "Print issue JSON",
      (label, menu) =>
        console.log(issue)
    actionsMenu.add "Print transition JSON",
      (label, menu) ->
        console.log(tlist)

    actionsMenu.createStream().pipe(process.stdout)

# Shows a list of issues
# On selection the showActionPage method is called
module.exports.showIssueSelection = showIssueSelection = (issues, cb) ->
  issuesMenu = createMenu()
  issuesMenu.write("Please, select an action:\n")
  issuesMap = {}
  _.each issues.issues,
    (issue, idx) ->
      label = "#{issue.fields.summary} (#{issue.key})"
      issuesMap[label] = issue
      issuesMenu.add(label)
  issuesMenu.on "select", (label) =>
    issuesMenu.close()
    cb(null, issuesMap[label])

  issuesMenu.createStream().pipe(process.stdout);
