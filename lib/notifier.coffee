_ = require 'lodash'
CFG =require('config')
Notification = require('node-notifier')
storage = require './storage'

notifier = new Notification()
notify = ->
  if not _.isUndefined storage.getItem('current')
    message = "You are working on issue #{storage.getItem('current').key} since #{storage.getItem('current').started}"
  else
    message = "No timer for your current work"
  notifier.notify({
      message: message
  })
setInterval(notify, CFG.Server.notificationTimer)
