CFG =require('config')
JiraApi = require('jira').JiraApi
logger = require 'winston'
_ = require 'lodash'
Duration = require 'duration'
moment = require 'moment'
storage = require './storage'

buildJQL = (conditions, order=null) ->
  stringify = _.map(conditions, (value, key) ->
    if _.isArray(value)
      value = "(#{JSON.stringify(value).slice(1,-1)})"
      "#{key.toLowerCase()} in #{value}"
    else
      "#{key.toLowerCase()}=#{JSON.stringify(value)}"
    )
  stringify = stringify.join(' AND ')
  if order
    stringify += " ORDER BY #{order}"
  logger.debug(stringify)
  return stringify

class JiraTimer

  constructor: (@jiraConfig, @userConfig, @oauthConfig=false) ->
    @jira = new JiraApi(jiraConfig.protocol, jiraConfig.server, jiraConfig.port,
                userConfig.login, userConfig.password, jiraConfig.apiversion, true, true, oauthConfig)

  searchIssues: (conditions, order, cb) ->
    @searchJQL(buildJQL(conditions, order), cb)

  searchJQL: (jql, cb) ->
    @jira.searchJira(jql, {}, cb)

  getIssue: (key, cb) ->
    logger.debug("Looking up issue #{key}")
    @jira.findIssue key, cb

  # Moves the issue to In Progress, and starts the timer
  # @param key JIRA issue key
  startWork: (key, cb) ->
    if storage.getItem('current')?.key
      return cb(new Error "There is a running job. Stop it first. #{storage.getItem("current").key}")

    logger.debug("Starting work on #{key}")
    @jira.transitionIssue key, {
      transition:
        id: CFG.Transitions.WorkStartedID
      },
      (err, status) ->
        resp = {
          status: "success",
          messages: []
        }
        if err
          logger.error(err)
          resp.messages.push "Could not transition to In Progress. Is it already there?"
        else
          resp.messages.push "Issue transitioned to In Progress"
        # start the timer
        storage.setItem('current', {
          key: key
          started: Date.now()
        })
        resp.messages.push "Started timer on #{key}"
        cb(null, resp)

  # Log work on the given issue
  # @param key JIRA issue key
  # @param remaining The remaining time to set,
  #   if omitted the remaining time is adjusted automatically
  _logWork: (key, newEstimate=false, cb) ->
    if not storage.getItem('current')
      return cb new Error("No running timer")

    else if storage.getItem('current').key != key
      return cb new Error("You can not log work for an issue that was not measured")

    logger.debug("Logging work on #{key}")
    if newEstimate
      logger.debug("New estimate is #{newEstimate}")
      
    now = Date.now()
    started = new Date(unix_timestamp=storage.getItem('current').started)
    worked = {
      started: moment(started).format("YYYY-MM-DDTHH:mm:ss.SSSZZ")
    }
    duration = new Duration(started, new Date(unix_timestamp=now))
    worked.timeSpent = duration.toString(1, 2)  # smaller than minutes are rejected

    @jira.addWorklog key, worked, newEstimate,
      (err, status) ->
        resp = {
          status: "success"
          messages: []
        }
        if(err)
          logger.error(err)
          resp.messages.push "Adding worklog of #{worked.timeSpent} failed. Please, do it online: https://vidzor.atlassian.net/secure/TempoAddHours!default.jspa?key=#{key}"
        else
          resp.messages.push "#{worked.timeSpent} were registered on task #{key}"

        storage.removeItem('current')
        resp.messages.push "The timer was cleared"
        cb(null, resp)

  # Moves the issue to Reopened, and stops the timer
  # @param key JIRA issue key
  # @param newEstimate The remaining time to set,
  #   if omitted the remaining time is adjusted automatically
  stopWork: (key, newEstimate=false, cb) ->
    logger.debug("Stopping work on #{key}.")
    if newEstimate
      logger.debug("New estimate is #{newEstimate}")
    @getIssue key, (err, issue) =>
      if err
        logger.error(err)
        logger.debug err
        cb(err)
      else
        # if the issue is already resolved, then move it to reopened, and register the time
        if issue.fields.resolution
          # log the work
          @_logWork key, newEstimate, cb
        else
          logger.debug("Transitioning issue to #{CFG.Transitions.WorkStoppedID}")
          @jira.transitionIssue key,
            transition:
              id: CFG.Transitions.WorkStoppedID
            (err, status) =>
              # resp = {
              #   status: "success"
              #   messages: []
              # }
              # console.log("here")
              if err
                logger.error(err)
              else
                resp.messages.push "Issue #{key} was moved to reopen."

              # cb_wrapped = (err, resp2) ->
              #   console.log(err)
              #   console.log(resp2)
              #   console.log(resp)
              #   resp.messages.extend(resp2.messages)
              # log the work
              @_logWork key, newEstimate, cb

  # Stops the current timer, and registers the time
  # @param newEstimate The remaining time to set,
  #   if omitted the remaining time is adjusted automatically
  stopCurrentWork: (newEstimate=false) ->
    if storage.getItem('current')
      @stopWork(storage.getItem('current').key, newEstimate)
    else
      console.log("No active timer found")

  # Cancels the current timer without registering any time spent.
  cancelCurrentWork: () ->
    storage.removeItem('current')
    console.log("Previous counter was cancelled")

  getCurrent: () ->
    return storage.getItem('current')

module.exports = (jiraConfig, userConfig, oauthConfig) ->
  new JiraTimer(jiraConfig, userConfig, oauthConfig)
