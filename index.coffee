#!/usr/bin/env coffee
CFG =require('config')
createMenu = require('terminal-menu')
_ = require('lodash')
logger = require 'winston'
logger.setLevels(logger.config.syslog.levels);

# setting up argument processing
program = require('commander');
list = (val) ->
  val.split(',')
program
  .version('0.0.1')
  .usage("jiratimer.coffee <action>. \n\n
  Where action is one of:\n
  * kanban - shows the list of ToDo tasks\n
  * current - shows the list of tasks In Progress\n
  * show <key> - shows the page of the specified issue\n
  * info - shows the local timer status\n
  * stop - stops the current timer session
  ")
  .option('-d, --daemon', 'Runs in daemon mode for notification')
  .option('-t, --issue-type <list>', 'Issue types', list)
  .option('-s, --issue-status <list>', 'Issue types', list)
  .parse(process.argv);

if program.issueStatus
  CFG.Issues.status = program.issueStatus
if program.issueType
  CFG.Issues.issueType = program.issueType

actions = require('./lib/actions')

run_action = (action, args=null) ->
  # clear the terminal
  process.stdout.write '\u001B[2J\u001B[0;0f'
  if actions[action]
    actions[action](args)
  else
    logger.warning("No action #{action}")
    program.help()

if program.daemon
  require('./lib/daemon')
else if not program.args
  program.help()
else
  run_action program.args[0], program.args.slice(1)
