chai = require('chai')
sinon = require 'sinon'
sinonChai = require 'sinon-chai'
chai.use(sinonChai)

expect = chai.expect

rewire = require('rewire')
storage = rewire '../lib/storage'
storage.__get__('storage').initSync({
  dir: __dirname + "/persist"
})

actions = rewire '../lib/actions'

describe "stopWork", ->
  callDaemon = null

  before ->
    callDaemon = sinon.spy()
    actions.__set__ "callDaemon", callDaemon

  after ->
    callDaemon.reset()

  it "calls /issues/key/stop", ->
    stopWork = actions.__get__ "stopWork"
    stopWork("MY-1")
    expect(callDaemon).to.have.been.called
    expect(callDaemon.args[0][0]).to.be.equal '/issues/MY-1/stop'

  describe "if remaining is falsy", ->

    it "it adds the query string", ->
      stopWork = actions.__get__ "stopWork"
      stopWork "MY-1", "1h"
      expect(callDaemon).to.have.been.called
      expect(callDaemon.args[1][0]).to.be.equal '/issues/MY-1/stop?new=1h'
