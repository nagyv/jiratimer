chai = require('chai')
sinon = require 'sinon'
sinonChai = require 'sinon-chai'
chai.use(sinonChai)

expect = chai.expect

rewire = require('rewire')
CFG =require('config')
CFG.Server.notificationTimer = 0
daemon = rewire '../lib/daemon'

request = require('request')
rq = "http://#{CFG.Server.interface}:#{CFG.Server.port}"

describe 'daemon', ->
  JT = null
  beforeEach ->
    JT = daemon.__get__('jiratimer')

  describe "/storage/key", ->
    it "looks up key in the storage", (done) ->
      storage = daemon.__get__ 'storage'
      sinon.stub(storage, 'getItem').returns("ok")
      request "#{rq}/storage/key", (err, resp, body) ->
        expect(storage.getItem).to.have.been.called
        expect(storage.getItem.args[0][0]).to.be.equal('key')
        expect(err).to.be.null
        expect(body).to.be.equal("\"ok\"")
        storage.getItem.reset()
        done()

  describe "/issues", ->
    it "returns the result of searchIssues", (done) ->
      sinon.stub JT, 'searchIssues', (what, order, cb) ->
        cb(null, "hello")

      request "#{rq}/issues", (err, resp, body) ->
        expect(JT.searchIssues).to.have.been.called
        callArgs = JT.searchIssues.args[0]
        expect(callArgs[0]).to.be.equal CFG.Issues
        expect(callArgs[1]).to.be.equal "Rank ASC"
        expect(err).to.be.null
        expect(resp.statusCode).to.be.equal 200
        JT.searchIssues.reset()
        done()
        
    describe "/:issue", ->
      it "returns the given issue", (done) ->
        sinon.stub JT, "getIssue", (key, cb) ->
          cb(null, "hello")

        request "#{rq}/issues/MY-1", (err, resp, body) ->
          expect(JT.getIssue).to.have.been.called
          callArgs = JT.getIssue.args[0]
          expect(callArgs[0]).to.be.equal "MY-1"
          expect(err).to.be.null
          expect(resp.statusCode).to.be.equal 200
          JT.getIssue.reset()
          done()

      describe "/transitions", ->
        it "returns the issue's transitions", (done) ->
          sinon.stub JT.jira, "listTransitions", (key, cb) ->
            cb(null, "hello")

          request "#{rq}/issues/MY-1/transitions", (err, resp, body) ->
            expect(JT.jira.listTransitions).to.have.been.called
            callArgs = JT.jira.listTransitions.args[0]
            expect(callArgs[0]).to.be.equal "MY-1"
            expect(err).to.be.null
            expect(resp.statusCode).to.be.equal 200
            JT.jira.listTransitions.reset()
            done()

      describe "/start", ->
        it "starts work", (done) ->
          sinon.stub JT, "startWork", (key, cb) ->
            cb(null, "hello")

          request "#{rq}/issues/MY-1/start", (err, resp, body) ->
            expect(JT.startWork).to.have.been.called
            callArgs = JT.startWork.args[0]
            expect(callArgs[0]).to.be.equal "MY-1"
            expect(err).to.be.null
            expect(resp.statusCode).to.be.equal 200
            JT.startWork.reset()
            done()

      describe "/stop", ->
        before ->
          sinon.stub JT, "stopWork", (key, estimate, cb) ->
            cb(null, "hello")

        it "stops work", (done) ->
          request "#{rq}/issues/MY-1/stop", (err, resp, body) ->
            expect(JT.stopWork).to.have.been.called
            callArgs = JT.stopWork.args[0]
            expect(callArgs[0]).to.be.equal "MY-1"
            expect(callArgs[1]).to.be.false
            expect(err).to.be.null
            expect(resp.statusCode).to.be.equal 200
            done()

        it "passes on remaining time", (done) ->
          request "#{rq}/issues/MY-1/stop?new=1h", (err, resp, body) ->
            expect(JT.stopWork).to.have.been.called
            callArgs = JT.stopWork.args[1]
            expect(callArgs[0]).to.be.equal "MY-1"
            expect(callArgs[1]).to.be.equal "1h"
            expect(err).to.be.null
            expect(resp.statusCode).to.be.equal 200
            done()

        after ->
          JT.stopWork.reset()
