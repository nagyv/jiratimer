rewire = require('rewire')
CFG =require('config')
jiratimer = rewire('../lib/jiratimer.coffee')

moment = require('moment')

jiratimer.__get__('storage').initSync({
  dir: __dirname + "/persist"
})

describe "buildJQL", ->
  it "escapes all values", ->
    res = jiratimer.__get__('buildJQL')({
      'key': 'val1'
      })
    expect(res).toEqual('key="val1"')

  it "handles array lookups", ->
    res = jiratimer.__get__('buildJQL')({
      'key': ['val1']
      })
    expect(res).toEqual('key in ("val1")')

  it "handles order by", ->
    res = jiratimer.__get__('buildJQL')({
      'key': 'val1'
      }, 'ordering')
    expect(res).toEqual('key="val1" ORDER BY ordering')


describe "JiraTimer", ->
  beforeEach ->
    jiratimer.__get__('storage').clear()

  it "constructs a jira api instance", ->
    JT = jiratimer {
      protocol: 'https',
      host: 'example.jira.com',
      port: '443',
      version: 'latest'
    }, {
      login: 'a',
      passwrod: 'b'
    }
    expect(JT.jira).toBeDefined()
  describe 'searchJQL', ->
    JT = null
    beforeEach ->
      JT = jiratimer {
        protocol: 'https',
        host: 'example.jira.com',
        port: '443',
        version: 'latest'
      }, {
        login: 'a',
        passwrod: 'b'
      }
      spyOn JT.jira, 'searchJira'

    it 'calls JiraAPI.searchJira', ->
      JT.searchJQL("query", "cb")
      expect(JT.jira.searchJira).toHaveBeenCalledWith("query", {}, "cb")

    it 'calls the callback with err, result', ->
      JT.jira.searchJira.andCallFake (query, options, cb) ->
        cb("err", "result")
      err = result = null
      JT.searchJQL("query", (_err, _result) ->
        err = _err
        result = _result
        )
      waits 0
      runs ->
        expect(err).toEqual("err")
        expect(result).toEqual("result")

  describe 'searchIssues', ->
    JT = null
    beforeEach ->
      JT = jiratimer {
        protocol: 'https',
        host: 'example.jira.com',
        port: '443',
        version: 'latest'
      }, {
        login: 'a',
        passwrod: 'b'
      }
      spyOn JT, 'searchJQL'

    it 'calls searchJQL', ->
      JT.searchIssues {key: 'val'}, "ordering", "cb"
      expect(JT.searchJQL).toHaveBeenCalledWith('key="val" ORDER BY ordering', "cb")

    it 'calls the callback with err, result', ->
      JT.searchJQL.andCallFake (jql, cb) ->
        cb("err", "result")
      err = result = null
      JT.searchIssues {key: 'val'}, "ordering", (_err, _result) ->
        err = _err
        result = _result
      waits 0
      runs ->
        expect(err).toEqual("err")
        expect(result).toEqual("result")

  describe 'getIssue', ->
    JT = null
    beforeEach ->
      JT = jiratimer {
        protocol: 'https',
        host: 'example.jira.com',
        port: '443',
        version: 'latest'
      }, {
        login: 'a',
        passwrod: 'b'
      }
      spyOn JT.jira, 'findIssue'

    it "calls findIssue", ->
      JT.getIssue "MY-1", "cb"
      expect(JT.jira.findIssue).toHaveBeenCalledWith("MY-1", "cb")

    it 'calls the callback with err, result', ->
      JT.jira.findIssue.andCallFake (jql, cb) ->
        cb("err", "result")
      err = result = null
      JT.getIssue "MY-1", (_err, _result) ->
        err = _err
        result = _result
      waits 0
      runs ->
        expect(err).toEqual("err")
        expect(result).toEqual("result")

  describe "startWork", ->
    JT = null
    beforeEach ->
      JT = jiratimer {
        protocol: 'https',
        host: 'example.jira.com',
        port: '443',
        version: 'latest'
      }, {
        login: 'a',
        passwrod: 'b'
      }
      jiratimer.__get__('storage').removeItem('current')
      spyOn(JT.jira, "transitionIssue")

    it "transitions the issue to InProgressID", ->
      JT.startWork "MY-1"
      expect(JT.jira.transitionIssue).toHaveBeenCalled()
      callArgs = JT.jira.transitionIssue.calls[0].args.splice(0, 2)  # drop the callback
      expect(callArgs).toEqual [
        "MY-1",
        {
          transition:
            id: CFG.Transitions.WorkStartedID
        }
      ]

    describe "when successful", ->

      beforeEach ->
        JT.jira.transitionIssue.andCallFake (key, data, cb) ->
          cb(null, 'Success')

      it "should set the current item", ->
        storage = jiratimer.__get__('storage')
        spyOn(storage, 'setItem')
        JT.startWork "MY-1", ->

        waits(0)
        runs ->
          expect(storage.setItem).toHaveBeenCalled()

      it 'calls the callback with "result: success"', ->
        err = "err"
        result = null
        JT.startWork "MY-1", (_err, _result) ->
          err = _err
          result = _result
        waits 0
        runs ->
          expect(err).toEqual(null)
          expect(result.status).toEqual("success")

    it "should return with status:error if there is a running timer", ->
      storage = jiratimer.__get__('storage')
      storage.setItem('current', {key: "MY-1"})
      err = null
      JT.startWork "MY-1", (_err) -> err = _err
      expect(err).not.toEqual(null)

  describe "_logWork", ->
    JT = storage = null
    beforeEach ->
      JT = jiratimer {
        protocol: 'https',
        host: 'example.jira.com',
        port: '443',
        version: 'latest'
      }, {
        login: 'a',
        password: 'b'
      }
      storage = jiratimer.__get__('storage')
      storage.setItem "current", {
        key: "MY-1",
        started: 1397400000000
      }
      spyOn JT.jira, "addWorklog"
      spyOn(Date, 'now').andReturn(1397403600000)

    it "to throw on error if no timer", ->
      storage.removeItem 'current'
      expect(-> JT._logWork("MY-1")).toThrow()

    it "to throw an error if key is not the current work", ->
      expect(-> JT._logWork("MY-2")).toThrow()

    it "to add Worklog", ->
      JT._logWork "MY-1", ->
      expect(JT.jira.addWorklog).toHaveBeenCalled()
      callArgs = JT.jira.addWorklog.calls[0].args.splice(0, 2)  # drop the callback
      expect(callArgs).toEqual [
        "MY-1",
        {
          started: '2014-04-13T16:40:00.000+0200',
          timeSpent: "1h 0m"
        }
      ]

    it "passes on the remaining time", ->
      JT._logWork "MY-1", "1h", ->
      expect(JT.jira.addWorklog).toHaveBeenCalled()
      callArgs = JT.jira.addWorklog.calls[0].args.splice(0, 3)  # drop the callback
      expect(callArgs).toEqual [
        "MY-1",
        {
          started: '2014-04-13T16:40:00.000+0200',
          timeSpent: "1h 0m"
        },
        "1h"
      ]

    it 'calls the callback with err, result', ->
      JT.jira.addWorklog.andCallFake (key, worked, newEst, cb) ->
        cb("err", "result")
      err = "err"
      result = null
      JT._logWork "MY-1", "auto", (_err, _result) ->
        err = _err
        result = _result
      waits 0
      runs ->
        expect(err).toEqual(null)
        expect(result.status).toEqual("success")

  describe "stopWork", ->
    JT = storage = null
    beforeEach ->
      JT = jiratimer {
        protocol: 'https',
        host: 'example.jira.com',
        port: '443',
        version: 'latest'
      }, {
        login: 'a',
        password: 'b'
      }
      spyOn(JT, '_logWork').andCallFake (key, newEst, cb) ->
        cb null, {status: "success"}

    it "looks up the issue", ->
      spyOn JT, 'getIssue'
      JT.stopWork("MY-1")
      expect(JT.getIssue).toHaveBeenCalled()
      expect(JT.getIssue.calls[0].args[0]).toEqual("MY-1")

    describe "if issue is resolved", ->

      beforeEach ->
        spyOn(JT, 'getIssue').andCallFake (key, cb) ->
          cb null, {
            fields:
              resolution: true
          }

      it "calls _logWork", ->
        cb = jasmine.createSpy('callback')
        JT.stopWork "MY-1", false, cb
        expect(JT._logWork).toHaveBeenCalledWith("MY-1", false, cb)

      it 'calls the callback with err, result', ->
        cb = jasmine.createSpy('callback')
        JT.stopWork "MY-1", false, cb
        waits 0
        runs ->
          expect(cb).toHaveBeenCalled()
          expect(cb.calls[0].args[0]).toEqual(null)
          expect(cb.calls[0].args[1].status).toEqual("success")

      it "passes on the remaining time", ->
        JT.stopWork "MY-1", "1h", ->
        callArgs = JT._logWork.calls[0].args.splice(0, 2)  # drop the callback
        expect(callArgs).toEqual ["MY-1", '1h']

    describe "if issue is not resolved", ->
      beforeEach ->
        spyOn( JT, 'getIssue').andCallFake (key, cb) ->
          cb null, {
            fields:
              resolution: false
          }

      it "calls _logWork", ->
        cb = ->
        JT.stopWork("MY-1", false, cb)
        expect(JT._logWork).toHaveBeenCalledWith("MY-1", false, cb)

      it "reopens the issue", ->
        spyOn JT.jira, 'transitionIssue'
        JT.stopWork "MY-1", false, ->
        expect(JT.jira.transitionIssue).toHaveBeenCalled()
        callArgs = JT.jira.transitionIssue.calls[0].args.splice(0, 2)  # drop the callback
        expect(callArgs).toEqual [
          "MY-1",
          {
            transition:
              id: CFG.Transitions.WorkStoppedID
          }
        ]

      it "passes on the remaining time", ->
        JT.stopWork "MY-1", "1h", ->
        callArgs = JT._logWork.calls[0].args.splice(0, 2)  # drop the callback
        expect(callArgs).toEqual ["MY-1", '1h']

  describe "stopCurrentWork", ->
    JT = storage = null
    beforeEach ->
      JT = jiratimer {
        protocol: 'https',
        host: 'example.jira.com',
        port: '443',
        version: 'latest'
      }, {
        login: 'a',
        password: 'b'
      }
      spyOn JT, 'stopWork'
      jiratimer.__get__('storage').setItem('current', {
        key: "MY-1"
      })

    it "calls stopWork with the current ID", ->
      JT.stopCurrentWork()
      expect(JT.stopWork).toHaveBeenCalledWith("MY-1", false)

    it "passes on the remaining time", ->
      JT.stopCurrentWork("1h")
      expect(JT.stopWork).toHaveBeenCalledWith("MY-1", '1h')

  describe "getCurrent", ->
    JT = storage = null
    beforeEach ->
      JT = jiratimer {
        protocol: 'https',
        host: 'example.jira.com',
        port: '443',
        version: 'latest'
      }, {
        login: 'a',
        password: 'b'
      }
    it "returns the current timer data", ->
      jiratimer.__get__('storage').setItem('current', 'hello')
      res = JT.getCurrent()
      expect(res).toEqual('hello')
