module.exports =
  JIRA:  # settings to access the JIRA instance
    protocol: "https"
    server: "vidzor.atlassian.net"
    port: 443
    apiversion: "latest"
  Issues:  # this dict provides the default query data
    project: "VIDZOR"
    status: ["Open", "Reopened"]
    issueType: ["Bug", "Improvement", "New Feature", "Story", "Task", "Sub-task"]
  Server:  # settings to run the daemon
    interface: 'localhost'
    port: 8080
    notificationTimer: 15*3600  #settings to 0 means no notifications
  Transitions:  # settings for the transitions
    WorkStartedID: 4  # In Progress
    WorkStoppedID: 301 # Reopen
    showTransitions: false
